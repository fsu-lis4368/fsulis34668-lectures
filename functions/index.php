<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Test</h1>
        
        <?php
        
            // ---------------------------------
            
            // Define a function
            function addSomeNumbers($a, $b)
            {
                echo $a + $b;
            }
        
            // ----------------------------------
            
            // A second function
            function doSomeMath($a, $b, $c)
            {
               $sum  = $a + $b + $c;
               $prod = ($sum * 2 % 1.5);
               $more = ($prod + $sum + 12);
               
               echo "<p>Result of math: " . $more . "</p>";
            }
            
            // ----------------------------------
            
            // Call the function down here
            echo 'calling function...';
            addSomeNumbers(5, 15);
            addSomeNumbers(2, 12);
            addSomeNumbers(3, 20);
            
            doSomeMath(2, 3, 4);
            doSomeMath(10, 11, 12);
            ?>
    </body>
</html>
