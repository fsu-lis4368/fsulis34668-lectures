<?php

//
// THE FIVE COMMANDMENTS OF FUNCTIONS
//

//
// 1.Thou shalt not allow functions to do more than one thing
//


// BAD:
function doSomeMath($a, $b)
{
    $result = $a + $b;
    
    echo "The result is " . $result;
}

// GOOD:
function doSomeMath($a, $b)
{
    $result = $a + $b;
    
    return $result;
}

echo "The value is: " . doSomeMath(5, 2);

//
// 2. Thou shalt always return the same type of variable from the function
//

// Bad -- It returns different TYPES of variables for different conditions
function getSomeNames($which)
{
    if ($which === 'a') {
        return ['apple', 'aardvark', 'asparagus'];
    }
    elseif ($which == 'b') {
        return 'banana';
    }
    else {
        return null;
    }
}


// GOOD - Because we always get the SAME type
function getSomeNames($which)
{
    if ($which === 'a') {
        return ['apple', 'aardvark', 'asparagus'];
    }
    elseif ($which == 'b') {
        return ['banana'];
    }
    else {
        return [];
    }
}


//
// 3. Thou shalt never accept too many arguments
//

// BAD - Refactor
function connectToDatabase($dbName, $dbUser, $dbPass, $dbPort, $dbHost, $dbSchema, $dbDriver)
{
    // do some stuff...
}

// We'll see GOOD later.



//
// 4. Thou shalt almost, err, sort of, never exceed 50 lines
//

// Example to come..

//
// 5. Thou shalt always use PHPDOC
//

// BAD - WTF does this do?
function rotateTheConfabulator($schema, $docRight, $fooBlaz = 5)
{
    // mystery code
}


/**
 * Rotate Confabulator does some magic stuff
 * 
 * Don't ask; don't tell.
 * 
 * @param string $schema
 * @param integer $docRight
 * @param integer $fooBlaz
 * @return array
 */
function rotateTheConfabulator($schema, $docRight, $fooBlaz = 5)
{
    // mystery code
}

