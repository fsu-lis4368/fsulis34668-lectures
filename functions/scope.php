<?php

// Global scope

$a = 'apple';
$b = 'banana';
$c = 'car';

// snake case
$a_yellow_banana = 'banana';

// camel case
$aYellowBanana = 'banana';

// -----------------------------------

// Everything inside is local scope
function tellMe($what)
{
    $a = 'aardvark';
    
    echo "This string is " . $what;
}

// -----------------------------------

// Another local scope
function tellMeMore($what, $what2)
{
    echo "Two strings" . $what . " --" . $what2;
}

// -----------------------------------

/**
 * Calculate the price of a ticket for a block of seats
 * 
 * @param int $numberOfSeats
 * $numberOfSeats@param float $taxRate        Local tax rate
 * @param float $pricePerSeat   Price per seat
 * @return float                The calculated price
 */
function calculateTicketPrice($numberOfSeats, $taxRate, $pricePerSeat = 4)
{
    // Multiply the number of seats by the price per seat
    $totalPrice = $numberOfSeats * $pricePerSeat;
    
    // Add 7.5 tax to it
    $finalPrice = $totalPrice + ($totalPrice * $taxRate);
    
    return $finalPrice;
}

// ------------------------------

/**
 * Determine if and which bonus package a customer gets
 * 
 * If the price is over 100$, we will give the customer a 
 * bonus package.  We randomly assign these packages based
 * on the number of seats (even or odd).
 * 
 * @param float $totalPrice
 * @param int   $numberOfSeats
 * @return string
 */
function determineBonusPackage($totalPrice, $numberOfSeats)
{   
    // provides package a if number of seats is even, else package b
    // only applies to ticket prices over $100
    if ($totalPrice > 100) {
        if ($numberOfSeats % 2 == 0) {
            return 'package a';
        }
        else {
            return 'package b';
        }
    } 
    else {
        return 'nothing';
    }
}

// ------------------------------

$orderNumSeats = 5;
$orderTaxRate  = 0.7;
$orderBaseSeat = 20;

$totalTicketPrice = calculateTicketPrice($orderNumSeats, $taxRate, $orderBaseSeat);

echo "Your total ticket price is " . $totalTicketPrice;


//$myArray = array('order1', 'order2', 'order3');

$myArray = [
    ['order1', 5,  20, 0.75],
    ['order2', 23, 15, 0.7 ],
    ['order3', 80, 12, 0.8 ],
    ['foobar', 20, 15, 0.9 ]
];

echo "<br/<br/>";

?>
<table>
    <thead>
        <tr>
            <th>Order Name</th>
            <th>Num seats</th>
            <th>Seat price</th>
            <th>Tax Rate</th>
            <th>Total Price</th>
            <th>Bonus Package</th>
        </tr>
    </thead>
    <tbody>
        
        <?php
            foreach ($myArray as $row) {
                
                $totalPrice = calculateTicketPrice($row[1], $row[3], $row[2]);

                
                echo "<tr>";
                echo "<td>" . $row[0] . "</td>";
                echo "<td>" . $row[1] . "</td>";
                echo "<td>" . $row[2] . "</td>";
                echo "<td>" . $row[3] . "</td>";
                echo "<td>" . $totalPrice . "</td>";
                echo "<td>" . determineBonusPackage($totalPrice, $row[1]). "</td>";
                
                echo "</tr>";
            }
         ?>
        
    </tbody>
</table>