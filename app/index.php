<?php

/**
 * Front Controller for my Application
 */

// 1. Bootstrap the Application

// Enable use of the session
session_start();
require('includes/functions.php');

// 2. Determine which page to load from the request

if (isset($_GET['page'])) {
    $pageToLoad = 'pages/' . $_GET['page'] . '.php';
}
else {
    $pageToLoad = 'pages/front.php';
}

// 3. Ennsure the page actually exists...
if (file_exists($pageToLoad) === false) {
    header("HTTP/1.0 404 Not Found");
    $pageToLoad = 'includes/404.php';
}


// 4. Load that page with common header and footer

require('includes/header.php');
require($pageToLoad);
require('includes/footer.php');
