<?php

    // Enable use of the session
    session_start();
    
    // Require files
    require('functions.php');
    require('models/User.php');
    require('models/UserMapper.php');

    $userMapper = new UserMapper();

    // If form was submitted, then process it.
    if (count($_POST) > 0) {

        $un = $_POST['un'];
        $pw = $_POST['pw'];
        
        try {
            $userObject = $userMapper->getUser($un);                    

            // Check to see if the user is in the database
            if ($pw == $userObject->getPassword()) {

                $_SESSION['is_logged_in'] = true;
                $_SESSION['username'] = $un;        
                header("Location: secure.php");
            }
            else {
                $msg = "Bad Password";
            } 
            
        } catch (Exception $ex) {
             $msg = "Invalid Username";
        }     
    }
    
    
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Login Page - Everybody can see this</h1>
        
        <?php if (isset($msg)) { echo "<p>" . $msg . "</p>"; } ?>
        
        <form method="post">
            <p>
                <label for="un">Username</label>
                <input type="text" id="un" name="un" />
            </p>
            <p>
                <label for="pw">Password</label>
                <input type="password" id="pw" name="pw" />
            </p>
            <p><button type="submit">log me in!</button></p>
        </form>
    </body>
</html>
