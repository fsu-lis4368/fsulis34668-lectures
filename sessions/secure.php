<?php

    // Enable use of the session
    session_start();
    
    // Require files
    require('functions.php');
    require('models/User.php');
    require('models/UserMapper.php');
    
    // Check to see if the user is logged in,
    // Else die so the page doesn't load.
    if ( ! isset($_SESSION['is_logged_in'])) {
        header("Location: login.php");
    }
    
    $userMapper = new UserMapper();
    $userList = $userMapper->getUsers();
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Only Logged-In Users Can See This</h1>
        
        <h2>User List</h2>
        
        <ol>
        <?php
        
        foreach ($userList as $user) {
            echo "<li>" . $user->getUsername() . "</li>";
        }
        
        ?>
        </ol>
    </body>
</html>
