<?php

require('functions.php');
require('classes/Customer.php');
require('classes/CustomerMapper.php');

$customerMapper = new CustomerMapper();
$rows = $customerMapper->getCustomers();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Customer List</title>
        
        <style type="text/css">
            body {
                font-family: sans-serif;
            }
            
            table td,
            table th {
                border: 1px solid #999;
                padding: 1em;
            }
        </style>
    </head>
    <body>
        
        <h1>Customer List Page</h1>
        
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody>
                <?php
                
                foreach ($rows as $row) {
                    echo "<tr>";
                    echo "<td>{$row->getName()}</td>";
                    echo "<td>{$row->getEmailAddress()}</td>";
                    echo "<td>{$row->getPhoneNumber()}</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
        
    </body>
</html>
