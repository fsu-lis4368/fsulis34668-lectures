<?php

class Customer
{
    protected $name;

    protected $emailAddress;

    protected $phoneNumber;
    
    // ---------------------------------------

    public function __construct($name, $emailAddress, $phoneNumber = '')
    {        
        $this->setName($name);
        $this->setEmailAddress($emailAddress);
        $this->setPhoneNumber($phoneNumber);
    }
    
    // ---------------------------------------
    
    public function getName() {
        return $this->name;
    }

    public function getEmailAddress() {
        return $this->emailAddress;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setEmailAddress($emailAddress) {
        $this->emailAddress = strtolower($emailAddress);
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }


    
}
