<?php

/**
 * Casey McLaughlin is the Author
 */
 
 // Don't worry about what this line does for now; I promise we'll cover it 
 // later on, but also don't delete it.
 header("Content-type: text/plain");

 
// ---------------------------------------------------
// Demonstrate how to generate output using the `echo` statement
//

echo 'hi there';
 
// ---------------------------------------------------
// Explain in a brief sentence what the assignment operator is, looks like
// and what it does.  Also, demonstrate the assignment operator in action.
//


// ---------------------------------------------------
// Create variables with all of the available built-in data-types in PHP
// except for `resource`
//

$string = 'my string';

$integer = 15;
$integer_2 = -243;

$float = 45.23;

$my_boolean = true;
$another_bool = false;

$my_object = new stdclass;

$a_null_value = null;

// ---------------------------------------------------
// Demonstrate use of the following arithmatic operators:
// +, -, *, /, %, ++, --
//
// For the last three operaters (%, ++, --), write a comment
// explaining what each does in plain english


// ---------------------------------------------------
// Explain what `type-casting` is in a brief sentence
//

// ---------------------------------------------------
// Perform the following type-casts and output the results
// Note what happens
//
// - Integer ==> float
// - Float (with decimal values) ==> integer
// - Integer ==> string
// - String ==> integer
// - String with number at beginning it ==> integer
// - String with number at end (after some text) ==> integer
// - String with text in it to boolean
// - Empty string to boolean
// - String with only spaces in it to boolean
// - Float value (0.00) to boolean

$my_super_variable = (double) "xyzzy";
echo "\n";
echo $my_super_variable;
echo "\n";
echo gettype($my_super_variable);



// ---------------------------------------------------
// Indicate the URL that can you go to as a reference 
// for type-casting (aka "type jugggling") in PHP
//

// ---------------------------------------------------
// Demonstrate how to use conditional control statements
//
// - `if` with no `else`
// - `if` with `else`
// - `if` with `elseif` and `else` (note: PHP uses `else if` and `elseif` interchangably)
//

// ---------------------------------------------------
// List the comparison operators in PHP,

// ----------------------------------------------------
// Demonstrate a `while` loop, and a `for` loop in PHP

for ($i = 0; $i < 5; $i++) {
    
    echo "\nI is " . $i;
    
}

// ----------------------------------------------------
// Demonstrate what an infinite loop looks like in PHP


// ----------------------------------------------------
// Demonstrate how to use logical operators in `if` conditions


// ----------------------------------------------------
// Demonstrate the difference between the `==` and `===` equality operators.
// Show two different `if/else` statements that have the same operands, but 
// generate different results using `==` and `===`

// ----------------------------------------------------
// Demonstrate how to use the negation operator in a conditional statement



// ----------------------------------------------------
// Demonstrate creation of an empty array into a new variable

$my_array = array();


// ----------------------------------------------------
// Create an incremntal array with values

$my_array = array('dog', 'cat', 'baseball');

// ----------------------------------------------------
// Add some more values to it

$my_array[] = 'hamburger';
$my_array[] = 'godzilla';

echo "\nArrays\n\n";

echo $my_array;

for ($i = 0; $i < 5; $i++) {
    echo "\n" . $my_array[$i];
}

// ----------------------------------------------------
// Output the count of the number of items in the array

// ----------------------------------------------------
// Output two of the items in the array using their keys

// ----------------------------------------------------
// Iterate through the array using a `while` loop and output
// each value

// ----------------------------------------------------
// Iterate through the array using a `for` loop and output
// each value


// ----------------------------------------------------
// Create an associative array with key/value pairs

$assoc_array = array('dog' => 'good', 'cat' => 'bad', 'godzilla' => 'great');


for ($x = 0; $x < 3; $x++) {
    echo $assoc_array[$x];
}

// ----------------------------------------------------
// Add some values to the associative array


// ----------------------------------------------------
// Iterate through the associative array using a `foreach`
// loop


// ----------------------------------------------------
// Access some values directly in the array

echo $assoc_array['dog'];
echo $assoc_array['cat'];

// ----------------------------------------------------
// Dump the contents of your arrays using `var_dump`
