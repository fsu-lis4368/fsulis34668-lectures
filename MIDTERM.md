### Midterm Study Guide

To prepare for the mid-term, please ensure you are familiar with the following concepts:

* HTTP
  - Definition of HTTP
  - Name some common HTTP clients
  - Name some common HTTP servers
  - Understand *get*, *post*, *put*, and *delete* HTTP verbs
    - Know which of these web browsers implement
  - What does the term 'stateless' mean in HTTP context?
  - Understand how to spy on and interpret HTTP request and response headers
* PHP Basics
  - Know the basic data types in PHP
  - Define 'typecasting' in plain English
  - Explain the difference between the '==' and '===' operators
  - Be able to list examples of the following operators:
    - Assignment
    - Comparison
    - Logical
    - Mathematical
    - String
  - Demonstrate use of the following types of arrays:
    - Numeric/Incremental
    - Associative
    - Multidimensional
* PHP Functions
  - Be able to write a simple function that accepts arguments and returns a value
  - Understand variable scope within functions
  - Be able to identify *global* vs *local* scope in code
* PHP OOP
  - Understand *instantiation*
  - Understand constructor methods
  - Understand *public* vs *private* methods
  - Understand how inheritance works
  - Understand the difference between *immutable* and *mutable* objects
* PHP I/O Processing
  - Understand how to use the `$_GET` and `$_POST` variables to collect user input
  - Be able to explain why accepting raw input from `$_GET` and `$_POST` is a security risk
