<?php

require('functions.php');
require('classes/Customer.php');
require('classes/CustomerMapper.php');

/**
 * Prints 'checked' if an item exists in the POST array and is equal
 * to the $itemValue
 * 
 * @param string $itemName
 * @param string $itemValue
 * @return string
 */
function isChecked($itemName, $itemValue, $output = 'checked')
{
    if (isset($_POST[$itemName]) && $_POST[$itemName] == $itemValue) {
        return $output;
    } 
    else {
        return '';
    }
}





// If the form was submitted, $_POST will not be empty, and we
// should process the submission.
if (count($_POST) > 0) {

    // Create a cusotmer from the from data
    try {
        $customer = new Customer($_POST['name'], $_POST['emailAddress'], $_POST['phoneNumber']);
        
        // Create a mapper
        $mapper = new CustomerMapper();

        // Pass the new customer object to the mapper to add it to the db
        $result = $mapper->addCustomerToDb($customer);

        // Result will be true or false
        if ($result == true) {
            $msg  = 'You have been registered!';
        }
        else {
            $msg = 'Something went wrong.  Contact an administrator.';
        }            
        
    } catch (Exception $ex) {
        $errorMsg = "Well, looks like something went wrong:" . $ex->getMessage();
    }

}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Form</h1>
        
        <?php if (isset($msg)) { echo $msg; }
              else { ?>
        
              <?php if (isset($errorMsg)) {
                
                  echo "<p>" . $errorMsg . "</p>";
                  
              } ?>
        
        <form method='post'>
            <p>
                <label for="name">Name</label>
                <input type="text" name="name" />
            </p>
            
            <p>
                <label for="password">Password</label>
                <input type="password"  name='pw' />
            </p>
            
            <p>
                <label for="email">Email Address</label>
                <input type="email" name="emailAddress" value="<?php if (count($_POST) > 0) { echo $_POST['emailAddress']; }?>" />
            </p>
            
            <p>
                <label for="phone">Phone Number</label>
                <input type="text"  name="phoneNumber" value="<?php if (count($_POST) > 0) { echo $_POST['phoneNumber']; }?>" />
            </p>            
            
            <p>
                <input type='radio' name='newsletter' value='Yes'   <?php echo isChecked('newsletter', 'Yes');   ?> /> Yes
                <input type='radio' name='newsletter' value='No'    <?php echo isChecked('newsletter',  'No');   ?> /> No
                <input type='radio' name='newsletter' value='Maybe' <?php echo isChecked('newsletter', 'Maybe'); ?> /> Maybe
            </p>
            
            <p>
                <input type='checkbox' name='agree' value='1' /> I Agree to Terms and Conditions
            </p>

            <p>
                <label>What is your favorite color</label>
                <select name='color'>
                    <option value='gr' <?php echo isChecked('color', 'gr', 'selected'); ?>GREEN</option>
                    <option value='ye' <?php echo isChecked('color', 'ye', 'selected'); ?> >YELLOW</option>
                </select>
            </p>
            
            <p><button type="submit">Submit Form</button></p>
        </form>
        
        <?php } ?>
    </body>
</html>







