<?php

/**
 * Description of CustomerMapper
 *
 * @author casey
 */
class CustomerMapper 
{
    /**
     * Get customers from the database.
     * 
     * @return Customer[]  A list of Customer Objects
     */
    public function getCustomers()
    {
        $dbConn = getDbConnection();

        $stmt = $dbConn->prepare("SELECT * FROM customers");
        $stmt->execute();        
       
        $outArray = array();
        
        // $stmt->fetch() always gets the NEXT row every time it is called..
        while ($row = $stmt->fetch()) {
            $outArray[] = new Customer($row['name'], $row['email'], $row['phoneNumber']);
        }
   
        return $outArray;
    }
    
    public function addCustomerToDb($customer)
    {
        $dbConn = getDbConnection();
        $stmt = $dbConn->prepare("INSERT INTO customers (name, email, phoneNumber) VALUES (?, ?, ?)");
        $result = $stmt->execute(array(
            $customer->getName(),
            $customer->getEmailAddress(),
            $customer->getPhoneNumber()
        ));
        
        return $result;        
    }
}
