<?php

class Order
{
    // Order number
    protected $orderNumber;

    // Created timestamp
    protected $created;

    // created
    // completed
    // processed
    // shipped    
    protected $status;
    
    protected $customer;
    
    public function __construct($number, $customer) 
    {
        $this->setStatus('created');
        $this->setOrderNumber($number);
        $this->setCustomer($customer);
        $this->created = new DateTime();        
    }
    
    public function getOrderNumber() {
        return $this->orderNumber;
    }

    public function getCreated() {
        return $this->created;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setOrderNumber($orderNumber) {
        $this->orderNumber = $orderNumber;
    }

    public function setCreated($created) {
        $this->created = $created;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getCustomer() {
        return $this->customer;
    }

    public function setCustomer($customer) {
        $this->customer = $customer;
    }


    
}