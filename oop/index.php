<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <h1>OOP</h1>
        
        <?php
        
            // Include the requisite files
            require('classes/Order.php');
            require('classes/Customer.php');
            require('classes/Seat.php');
            
            // Seats
            $allMySeats = array();
            
            for ($i = 1; $i <= 100; $i++) {
                $allMySeats[] = new Seat($i);
            }
            
            // Customers
            $allMyCustomers = array(
              new Customer("Sally Sue", 'ss@ex.com'),
              new Customer('Bobby Bow', 'bb@ex.com')
            );
            
            
            // First customer will order a seat
            $anOrder = new Order(100, $allMyCustomers[0]);
            var_dump($anOrder);
            
            
        ?>
    </body>
</html>
